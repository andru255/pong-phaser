import 'phaser';
import { GameScene } from './game/game.scene';

const config: Phaser.Types.Core.GameConfig = {
    title: 'PongTS',
    type: Phaser.AUTO,
    backgroundColor: "#ddd",

    // dimentions
    width: 800,
    height: 600,

    // scenes
    scene : GameScene,

    // physics
    physics: {
        default: 'arcade',
        arcade: {
            debug: true,
            debugBodyColor: 0xff00ff,
        }
    },

    parent: 'game',
};

window.addEventListener('load', ()=> {
    const game = new Phaser.Game(config);
});




