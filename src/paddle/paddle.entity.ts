import 'phaser';

export interface PaddleSettings {
    x: number;
    y: number;
    height: number;
    width: number;
    fillColor?: number;
    fillAlpha?: number;
    velocityY?: number;
    velocityMaxY?: number;
    acceleration?: number;
    friction?: number;
}

export class PaddleEntity extends Phaser.GameObjects.Sprite {

    public features: PaddleSettings = {
        x: 0,
        y: 0,
        width: 0,
        height: 0,
        fillColor: 0x00,
        fillAlpha: 0x11,
        velocityY: 0,
        velocityMaxY: 25,
        acceleration: 3,
        friction: 0.85
    }

    public shape: Phaser.GameObjects.Rectangle
        & { body: Phaser.Physics.Arcade.Body };

    constructor(
        public scene: Phaser.Scene,
        settings: PaddleSettings
    ) {
        super(scene, settings.x, settings.y, 'paddle');
        this.features = Object.assign({}, this.features, settings);
        this.shape = scene.add.rectangle(
            this.features.x,
            this.features.y,
            this.features.width,
            this.features.height,
            this.features.fillColor,
            this.features.fillAlpha
        ) as any;
        scene.physics.add.existing(this.shape);
        this.shape.setOrigin(0,0);
        this.shape.body.immovable = true;
    }

    update(keys: Phaser.Types.Input.Keyboard.CursorKeys) {
        this.listenEvents(keys);
        this.checkBounds();
    }

    private listenEvents(
        keys: Phaser.Types.Input.Keyboard.CursorKeys
    ) {
        if (keys.up.isDown) {
            this.shape.body.velocity.y -= this.features.acceleration;

            // limit the velocity
            if (this.shape.body.velocity.y < -this.features.velocityMaxY) {
                this.shape.body.velocity.y = -this.features.velocityMaxY;
            }

        }

        if (keys.down.isDown) {
            this.shape.body.velocity.y += this.features.acceleration;

            // limit the velocity
            if (this.shape.body.velocity.y > this.features.velocityMaxY) {
                this.shape.body.velocity.y = this.features.velocityMaxY;
            }
        }

        this.shape.body.velocity.y *= this.features.friction;
        this.shape.body.y += this.shape.body.velocity.y;
    }

    public checkBounds() {
        var maxY = window.Math.max(0, this.shape.body.y);
        this.shape.body.y = maxY;
        var minY = window.Math.min(this.scene.scale.canvas.height - this.shape.height, this.shape.body.y);
        this.shape.body.y = minY;
    }
} 