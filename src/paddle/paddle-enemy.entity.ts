import { PaddleEntity, PaddleSettings } from "./paddle.entity";

export class PaddleEnemyEntity extends PaddleEntity {
    private isMoveDown: boolean = false;
    private isMoveUp: boolean= false;

    constructor(
        public scene: Phaser.Scene,
        settings: PaddleSettings
    ) {
        super(scene, settings);
    }

    public updateWithBall(
        ballShape: Phaser.GameObjects.Rectangle & { body: Phaser.Physics.Arcade.Body }
    ) {
        const {y: ballY, height: ballHeight} = ballShape.body;

        if(Math.random() < 0.1 ) {
            this.isMoveDown = false;
            this.isMoveUp = false;

            if (ballY + ballHeight < this.shape.body.y + (this.shape.body.height/2)) {
                this.isMoveUp = true;
            } else if(ballY > this.shape.body.y + (this.shape.body.height/2)) {
                this.isMoveDown = true;
            }
        }

        this.move();
        this.checkBounds();
    }

    private move() {
        if (this.isMoveUp ) {
            this.shape.body.velocity.y -= this.features.acceleration;

            if(this.shape.body.velocity.y < -this.features.velocityMaxY) {
                this.shape.body.velocity.y = -this.features.velocityMaxY;
            }

        }  
        
        if(this.isMoveDown) {
            this.shape.body.velocity.y += this.features.acceleration;

            if(this.shape.body.velocity.y > this.features.velocityMaxY) {
                this.shape.body.velocity.y = this.features.velocityMaxY;
            }
        }

        this.shape.body.velocity.y *= this.features.friction;
        this.shape.body.y += this.shape.body.velocity.y;
    }
}