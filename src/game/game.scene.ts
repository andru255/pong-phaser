import 'phaser';
import { GameConfig } from './game.config';
import { PaddleEntity, PaddleSettings } from '../paddle/paddle.entity';
import { BallEntity, BallSettings } from '../ball/ball.entity';
import { PaddleEnemyEntity } from '../paddle/paddle-enemy.entity';
import { Physics } from 'phaser';

export class GameScene extends Phaser.Scene {
    private player: PaddleEntity;
    private enemy: PaddleEnemyEntity;
    private ball: BallEntity;

    // speed in the game
    private speed: number = Phaser.Math.GetSpeed(600, 3);

    constructor() {
        super(GameConfig);
    }

    public create() {
        // setup the paddles
        const playerSettings: PaddleSettings = {
            width: 20,
            height: 150,
            x: 0,
            y: ( this.scale.height/2 ),
            fillColor: 0x0000ff,
            acceleration: 3,
        };

        const enemySettings: PaddleSettings = {
            width: 20,
            height: 150,
            x: this.scale.width - 20,
            y: 0,
            fillColor: 0xff0000,
            acceleration: 1,
        };

        // setup the ball
        const ballSettings: BallSettings = {
            x: ( this.scale.width/2 ),
            y: ( this.scale.height/2 ),
            radius: 30,
            speed: 200
        }

        this.player = new PaddleEntity(this, playerSettings);
        this.enemy = new PaddleEnemyEntity(this, enemySettings);
        this.ball = new BallEntity(this, ballSettings);
        this.ball.create(this.speed);
    }

    public update(time: number, delta: number) {
        const keys = this.input.keyboard.createCursorKeys();

        // entity self-updates
        this.player.update(keys);
        this.enemy.updateWithBall(this.ball.shape);
        this.ball.update();

        //collision between game objects
        this.physics.world.collide(this.player.shape, this.ball.shape);
        this.physics.world.collide(this.enemy.shape, this.ball.shape);
    }
}  