import 'phaser';

export interface BallSettings {
    x: number;
    y: number;
    radius: number;
    fillColor?: number,
    fillAlpha?: number,
    speed?: number
}

export class BallEntity extends Phaser.GameObjects.Sprite{
    public shape: Phaser.GameObjects.Arc 
                        & { body: Phaser.Physics.Arcade.Body };

    private _settings: BallSettings = {
            x: 0,
            y: 0,
            radius: 0,
            speed: -10 * 10
    } 

    constructor(
        scene: Phaser.Scene,
        settings: BallSettings
    ) {
        super(scene, settings.x, settings.y, 'ball');
        this._settings = Object.assign({}, this._settings, settings);
        this.shape = scene.add.circle(
            this._settings.x,
            this._settings.y,
            this._settings.radius,
            0x000,
            0x0f0
        ) as any;

        scene.physics.add.existing(this.shape, false);
    }

    public create(worldSpeed: number) {

        // able to listen collisions
        this.shape.body.checkCollision.left = true;
        this.shape.body.checkCollision.right = true;

        this.shape.body.setBounce(1, 1);
        this.shape.body.setVelocity(
            this._settings.speed, 
            this._settings.speed 
        );
    }

    private checkBounds() {
        const {x, y} = this.shape.body.position;
        const {width: shapeWidth, height: shapeHeight} = this.shape.body;
        const {width: worldWidth, height: worldHeight} = this.scene.game.scale;

        // up || bottom
        if( ( y <= 0 ) || ( y + shapeHeight ) >= worldHeight ) {
            this.shape.body.velocity.y *= -1;
        }

        // left || right
        if(( x + shapeWidth ) <= 0 || ( x >= worldWidth)) {
            this.shape.body.position = new Phaser.Math.Vector2(
                this._settings.x,
                this._settings.y
            );
        }
    }

    public update(): void {
        this.checkBounds();
    }
}