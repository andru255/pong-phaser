const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CompressionPlugin = require('compression-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const dev = require('./webpack.config.js');
const merge = require('webpack-merge');

// ref: https://github.com/oivinds/VueTheme/blob/master/webpack.config.js
if (process.env.NODE_ENV === 'production') {
  console.log("building for production :D");
}

module.exports = merge(dev, {
  mode: 'production',
  devtool: 'source-map',
  plugins: [
    new CleanWebpackPlugin(),
    new CompressionPlugin({
      deleteOriginalAssets: true,
    }),
    new HtmlWebpackPlugin({
      title: 'Pong | Phaserjs + TS',
      chunks: ['vendor', 'app'],
      chunksSortMode: 'manual',
      minify: {
        removeAttributeQuotes: true,
        collapseWhitespace: true,
        html5: true,
        minifyCSS: true,
        minifyJS: true,
        minifyURLs: true,
        removeComments: true,
        removeEmptyAttributes: true
      },
      hash: true
    }),
  ]
});
